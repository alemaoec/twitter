# FakeTwitter API

# Documentação (API)

- Todas as chamadas são feitas à partir da URL https://morning-ridge-51091.herokuapp.com/ (Production)

## V1

### Autenticação

POST */auth/login*

##### REQUEST

```json
{
  "username": "Diogo",
  "password": "secret12"
} 
```

##### RESPONSE

```json
{
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJEaW9nbyIsInNjb3BlcyI6W3siYXV0aG9yaXR5IjoiUk9MRV9BRE1JTiJ9XSwiaXNzIjoiaHR0cDovL2RldmdsYW4uY29tIiwiaWF0IjoxNTM1MDIyMDcwLCJleHAiOjE1MzUwNDAwNzB9.H-mh11uwuYGW3gYJKj5gMAEZzCvsKqrP3BS_HRil_t4"
}
```

O token retornado deverá ser passado como header em cada requisição que necessite de autenticação.

Exemplo:

>Authorization: "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJEaW9nbyIsInNjb3BlcyI6W3siYXV0aG9yaXR5IjoiUk9MRV9BRE1JTiJ9XSwiaXNzIjoiaHR0cDovL2RldmdsYW4uY29tIiwiaWF0IjoxNTM0OTkyMDYxLCJleHAiOjE1MzUwMTAwNjF9.2yb9aiozYmTtMVAh3rJuQ9sS-G7OvhniAmJDt18dkhs"

A autenticação é OBRIGATÓRIA para todas as ações, exceto:

- Criar usuário
- Autenticar


### Criar usuário

POST */auth/register*

##### REQUEST

```json
{
  "username": "Diogo 2",
  "password": "secret122",
  "name": "Diogo Soares da Silva",
  "bio": "Cara sussa!"
}
```

##### RESPONSE

```json
{
    "id": 1,
    "username": "Diogo 2",
    "password": "$2a$10$JV13nx74Uo7kGEBUjdILAeIQA4Eh.gFvCQQCT6cLCuC6g6cN3sDpu",
    "name": "Diogo Soares da Silva",
    "bio": "Cara sussa!",
    "follows": [],
    "followings": []
}
```
### Pegar Usuario autenticado

GET */users/id*

##### RESPONSE

```json
{
    "id": 1,
    "username": "Diogo 2",
    "password": "$2a$10$JV13nx74Uo7kGEBUjdILAeIQA4Eh.gFvCQQCT6cLCuC6g6cN3sDpu",
    "name": "Diogo Soares da Silva",
    "bio": "Cara sussa!"
}
```

### Listar tweets

GET */tweets*

##### RESPONSE

```json
[
    {
        "id": 1,
        "tweet": "Meu Tweet show",
        "user": {
            "id": 1,
            "username": "Diogo 2",
            "password": "$2a$10$JV13nx74Uo7kGEBUjdILAeIQA4Eh.gFvCQQCT6cLCuC6g6cN3sDpu",
            "name": "Diogo Soares da Silva",
            "bio": "Cara sussa!",
            "follows": [],
            "followings": []
        }
    }
]
```

### Criar tweet

POST */tweets

##### REQUEST

```json
{
  "tweet": "Meu Tweet show"
}
```

##### RESPONSE

```json
{
    "id": 1,
    "tweet": "Meu Tweet show",
    "user": {
        "id": 1,
        "username": "Diogo 2",
        "password": "$2a$10$JV13nx74Uo7kGEBUjdILAeIQA4Eh.gFvCQQCT6cLCuC6g6cN3sDpu",
        "name": "Diogo Soares da Silva",
        "bio": "Cara sussa!",
        "follows": [],
        "followings": []
    }
}
```

### SEGUIR USUÁRIO

POST */users/{id}/follow

##### RESPONSE
```json
ok
```

### Deixar de seguir USUÁRIO

DELETE */users/{id}/unfollow

##### RESPONSE
```json
ok
```



