package com.example.twitter.facebook;

import java.util.List;

public class Feed {
    private List<Post> data;

    public List<Post> getData() {
        return data;
    }

    public void setData(List<Post> data) {
        this.data = data;
    }
}
