package com.example.twitter.facebook;

import java.util.List;

public class Facebook extends ApiBinding {

    private static final String GRAPH_API_BASE_URL = "https://graph.facebook.com/v2.12";

    public Facebook(String accessToken) {
        super("EAAeHzRR13ZC8BAE7RSPPTBnVF4cFwgwZBOdJnvmLgTZCPwXgKWFabREOutZApfvkZBej6YfT4iJSOoICOQk5CToLiIkal4aDKTvXVDWpxNmhstq4rRXeqtZCmBwy3FVKfOAyPE4d7KpWkJ0wjMPRkGrEmoY2DumPZCVled41SMRZA3hNxoPHWdDUWK5jKUYGuREZD");
    }

    public Profile getProfile() {
        return restTemplate.getForObject(GRAPH_API_BASE_URL + "/me", Profile.class);
    }

    public List<Post> getFeed() {
        return restTemplate.getForObject(GRAPH_API_BASE_URL + "/me/feed", Feed.class).getData();
    }

}