package com.example.twitter.facebook;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

public abstract class ApiBinding {

    protected RestTemplate restTemplate;

    public ApiBinding(String accessToken) {
        this.restTemplate = new RestTemplate();
        this.restTemplate.getInterceptors().add(jsonMimeInterceptor());
    }

    private ClientHttpRequestInterceptor jsonMimeInterceptor() {
        return new ClientHttpRequestInterceptor() {
            @Override
            public ClientHttpResponse intercept(HttpRequest request, byte[] bytes, ClientHttpRequestExecution execution) throws IOException {
                HttpHeaders headers = request.getHeaders();
                headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
                headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
                return execution.execute(request, bytes);
            }
        };
    }

}
