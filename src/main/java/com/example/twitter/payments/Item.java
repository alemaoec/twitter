package com.example.twitter.payments;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Item {
    private String description;
    private String quantity;
    @JsonProperty("price_cents")
    private long priceCents;

    public Item() {
        super();
    }

    public Item(String description, String quantity, long price) {
        super();
        this.description = description;
        this.quantity = quantity;
        this.priceCents = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public long getPriceCents() {
        return priceCents;
    }

    public void setPriceCents(long priceCents) {
        this.priceCents = priceCents;
    }
}