package com.example.twitter.payments;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.security.SecureRandom;

public class PaymentResult {
    private String pdf;
    private boolean success;
    private String url;
    @JsonProperty("invoice_id")
    private String invoiceId;

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }
}