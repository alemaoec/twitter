package com.example.twitter.payments;

import com.example.twitter.Constants;
import com.example.twitter.facebook.Profile;
import com.example.twitter.utils.RequestResponseLoggingInterceptor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

public class Iugu {
    private static final String IUGU_API_BASE_URL = "https://api.iugu.com/v1/";
    private RestTemplate restTemplate;

    public Iugu() {
        ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());

        this.restTemplate = new RestTemplate(factory);
        this.restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
    }

    public PaymentResult directChange(Change change) {
        HttpHeaders headers=new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Accept", "application/json");
        HttpEntity requestEntity=new HttpEntity(change, headers);
        try {
            return restTemplate.postForObject(IUGU_API_BASE_URL + "charge?api_token=" + Constants.IUGU_API_TOKEN, change, PaymentResult.class);
        } catch (HttpClientErrorException e){
            String errorResponse=((HttpStatusCodeException)e).getResponseBodyAsString();
            System.out.println("==================================================");
            System.out.println(errorResponse);
            System.out.println("==================================================");
            return  null;
        }
    }
}
