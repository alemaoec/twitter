package com.example.twitter.payments;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Address {
    @JsonProperty("zip_code")
    private String zipCode;
    private String number;

    public Address() {
        super();
    }

    public Address(String zipCode, String number){
        super();
        this.zipCode = zipCode;
        this.number = number;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
