package com.example.twitter.payments;

import java.util.ArrayList;
import java.util.List;

public class Change {
    private String email;
    private String method;
    private List<Item> items;
    private Payer payer;

    public Change() {
        super();
    }

    public Change(String email, String method, Item item, Payer payer){
        super();
        this.email = email;
        this.method = method;
        this.items = new ArrayList<Item>();
        this.items.add(item);
        this.payer = payer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Payer getPayer() {
        return payer;
    }

    public void setPayer(Payer payer) {
        this.payer = payer;
    }
}
