package com.example.twitter.payments;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Payer {
    @JsonProperty("cpf_cnpj")
    private String document;
    private String name;
    private Address address;

    public Payer(){
        super();
    }

    public Payer(String document, String name, Address address) {
        super();
        this.document = document;
        this.name = name;
        this.address = address;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
