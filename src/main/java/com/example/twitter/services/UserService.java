package com.example.twitter.services;

import com.example.twitter.models.User;

import java.util.List;

public interface UserService {
    User getUserById(long id);
    List<User> getAllUsers();
//    List<Long> getFolloweds(long id);
    User createUser(User user);
    void flushUser(User user);
    User getUserByUsername(String username);
}


