package com.example.twitter.services;

import com.example.twitter.models.Tweet;

import java.util.List;

public interface TweetService {
    Tweet getTweetById(long id);
    List<Tweet> getAllTweets();
    List<Tweet> getAllTweetsFromUser(long userId);
    List<Tweet> getAllTweetsFromUsers(List<Long> userId);
    Tweet createTweet(Tweet tweet);
    void deleteTweet(long id);
}
