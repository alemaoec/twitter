package com.example.twitter.services;

import com.example.twitter.models.Tweet;
import com.example.twitter.repositories.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("tweetService")
public class TweetServiceImp implements TweetService {
    @Autowired
    TweetRepository tweetRepository;

    @Override
    public Tweet getTweetById(long id) {
        Optional<Tweet> optionalTweet = tweetRepository.findById(id);
        if(optionalTweet.isPresent()) {
            return optionalTweet.get();
        }
        return null;
    }

    @Override
    public List<Tweet> getAllTweets() {
        return tweetRepository.findAll();
    }

    @Override
    public List<Tweet> getAllTweetsFromUser(long userId) {
        return tweetRepository.findAllByUserId(userId);
    }

    @Override
    public List<Tweet> getAllTweetsFromUsers(List<Long> userId) {
        return tweetRepository.findAllByUserIdIn(userId);
    }

    @Override
    public Tweet createTweet(Tweet tweet) {
        return tweetRepository.save(tweet);
    }

    @Override
    public void deleteTweet(long id) {
        tweetRepository.deleteById(id);
    }
}
