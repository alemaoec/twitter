package com.example.twitter.repositories;

import com.example.twitter.models.Tweet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("tweetRepository")
public interface TweetRepository extends JpaRepository<Tweet, Long> {
    List<Tweet> findAllByUserId(long userId);
    List<Tweet> findAllByUserIdIn(List<Long> usersId);
}
