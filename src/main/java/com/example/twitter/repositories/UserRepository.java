package com.example.twitter.repositories;

import com.example.twitter.models.Tweet;
import com.example.twitter.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
//    @Query(value = "select f.followed_id from follows j join user u where f.follower_id = :id and " )
//    List<Long> findAllFollowedFromUser(@Param("id") long id);
}