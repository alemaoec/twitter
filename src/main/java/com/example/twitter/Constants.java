package com.example.twitter;

public class Constants {
    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5*60*60;
    public static final String SIGNING_KEY = "twitter_app";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String IUGU_API_TOKEN = "94605f8b8322e1ff1b592d0228d47d49";
}
