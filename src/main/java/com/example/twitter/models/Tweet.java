package com.example.twitter.models;

import javax.persistence.*;

@Entity
public class Tweet {
    @Id
    @GeneratedValue
    private Long id;
    private String tweet;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    public Tweet(){
        super();
    }

    public Tweet(String tweet) {
        super();
        this.tweet = tweet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTweet() {
        return tweet;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
