package com.example.twitter.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue
    private long id;
    private String username;
    private String password;
    private String name;
    private String bio;

    @JsonProperty("tweets_count")
    public int getTweetsCount() {
        return tweets.size();
    }


    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "user", orphanRemoval = true)
    private List<Tweet> tweets = new ArrayList<>();

    public User() {
        super();
    }

    public User(String username, String name, String password) {
        super();
        this.username = username;
        this.password = password;
        this.name = name;
    }

//    @ManyToMany(cascade = {
//            CascadeType.PERSIST,
//            CascadeType.MERGE
//    })
//    @JoinTable(name = "follows",
//            joinColumns = @JoinColumn(name = "follower_id"),
//            inverseJoinColumns=@JoinColumn(name="followed_id")
//    )
//    @JsonIgnore
//    private List<User> follows = new ArrayList<>();

//    @ManyToMany(cascade = {
//            CascadeType.PERSIST,
//            CascadeType.MERGE
//    })
//    @JoinTable(name = "follows",
//            joinColumns = @JoinColumn(name = "followed_id"),
//            inverseJoinColumns=@JoinColumn(name="follower_id")
//    )
//    private List<User> followings= new ArrayList<>();
//
//    public void follow(User user) {
//        user.getFollowings().add(this);
//        follows.add(user);
//    }
//
//    public void unfoloow(User user) {
//        user.getFollowings().remove(this);
//        follows.remove(user);
//    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

//    public List<User> getFollows() {
//        return follows;
//    }
//
//    public void setFollows(List<User> follows) {
//        this.follows = follows;
//    }
//
//    public List<User> getFollowings() {
//        return followings;
//    }
//
//    public void setFollowings(List<User> followings) {
//        this.followings = followings;
//    }
}
