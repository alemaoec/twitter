package com.example.twitter.controllers;

import com.example.twitter.TwitterApplication;
import com.example.twitter.models.Tweet;
import com.example.twitter.models.User;
import com.example.twitter.services.TweetService;
import com.example.twitter.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TweetController {
    @Autowired
    private TweetService tweetService;
    @Autowired
    private UserService userService;
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/tweets", method = RequestMethod.GET)
    public List<Tweet> getTweets() {
//        User currentUser = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        return tweetService.getAllTweets();
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/tweets/{id}", method = RequestMethod.GET)
    public Tweet getTweet(@PathVariable("id") long id) {
        return tweetService.getTweetById(id);
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/tweets", method = RequestMethod.POST)
    public Tweet createTweet(@RequestBody Tweet requestBody) {
//        User currentUser = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
//        if(currentUser != null) {
//            requestBody.setUser(currentUser);
//        }
        return tweetService.createTweet(requestBody);
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/tweets/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteTweet(@PathVariable("id") long id) {
        tweetService.deleteTweet(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
