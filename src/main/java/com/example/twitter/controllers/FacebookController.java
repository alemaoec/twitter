package com.example.twitter.controllers;

import com.example.twitter.facebook.Facebook;
import com.example.twitter.facebook.Post;
import com.example.twitter.facebook.Profile;
import com.example.twitter.models.Tweet;
import com.example.twitter.models.User;
import com.example.twitter.payments.*;
import com.example.twitter.services.TweetService;
import com.example.twitter.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FacebookController {
    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @RequestMapping(value = "/facebook/auth", method = RequestMethod.POST)
    public User registerUser(@RequestBody Profile requestBody) {
        Facebook facebookCli = new Facebook(requestBody.getAccessToken());
        Profile profileFetch = facebookCli.getProfile();
        User toSave = new User(profileFetch.getId(), profileFetch.getName(), encoder.encode("secret"));
        return userService.createUser(toSave );
    }

    @RequestMapping(value = "/facebook/feed", method = RequestMethod.GET)
    public List<Post> feed(@RequestParam("accessToken") String token) {
        Facebook facebookCli = new Facebook(token);
        return facebookCli.getFeed();
    }

    @RequestMapping(value = "/facebook/payment", method = RequestMethod.POST)
    public PaymentResult payment(@RequestBody Change change) {
        List<Item> items = new ArrayList<Item>();
        items.add(new Item("Produto", "2", 5600));
        Address address = new Address("79115400", "142");
        Payer payer = new Payer("02645192130", "Diogo Soares", address);
        change.setItems(items);
        change.setPayer(payer);
        Iugu iuguCli = new Iugu();
        return iuguCli.directChange(change);
    }
}

