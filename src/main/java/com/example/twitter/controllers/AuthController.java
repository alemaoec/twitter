package com.example.twitter.controllers;

import com.example.twitter.TwitterApplication;
import com.example.twitter.models.AuthToken;
import com.example.twitter.models.User;
import com.example.twitter.services.UserService;
import com.example.twitter.utils.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;

@RestController
public class AuthController {
    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private BCryptPasswordEncoder encoder;
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/auth/register", method = RequestMethod.POST)
    public User registerUser(@RequestBody User requestBody) {
        requestBody.setPassword(encoder.encode(requestBody.getPassword()));
        return userService.createUser(requestBody);
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/auth/login", method = RequestMethod.POST)
    public ResponseEntity<?> loginUser(@RequestBody User requestBody) throws AuthenticationException {
        TwitterApplication.logger.info("request");
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        requestBody.getUsername(),
                        requestBody.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final User userAuthenticatedUser = userService.getUserByUsername(requestBody.getUsername());
        final String token = jwtTokenUtil.generateToken(userAuthenticatedUser);
        return ResponseEntity.ok(new AuthToken(token));
    }
}
