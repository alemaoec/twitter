package com.example.twitter.controllers;

import com.example.twitter.TwitterApplication;
import com.example.twitter.models.Tweet;
import com.example.twitter.models.User;
import com.example.twitter.services.TweetService;
import com.example.twitter.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/users/{id}/follow", method = RequestMethod.POST)
    public ResponseEntity followUser(@PathVariable("id") long id) {
        User currentUser = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        User followed = userService.getUserById(id);
//        currentUser.follow(followed);
        userService.flushUser(currentUser);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public User getUser(@PathVariable("id") String id) {
        User currentUser = null;
        boolean isNumeric = id.chars().allMatch( Character::isDigit );
        if(isNumeric) {
            currentUser = userService.getUserById(Long.getLong(id));
        } else {
            currentUser = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return currentUser;
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/users/{id}/unfollow", method = RequestMethod.DELETE)
    public ResponseEntity unfollowUser(@PathVariable("id") long id) {
        User currentUser = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        User followed = userService.getUserById(id);
//        currentUser.unfoloow(followed);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    private static boolean isNumber(String number) {
        try {
            int value = Integer.valueOf(number);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}