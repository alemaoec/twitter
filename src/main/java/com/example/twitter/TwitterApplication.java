package com.example.twitter;

import com.example.twitter.models.Tweet;
import com.example.twitter.repositories.TweetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class TwitterApplication {

	public static final Logger logger = LoggerFactory.getLogger(TwitterApplication.class);


	public static void main(String[] args) {
		SpringApplication.run(TwitterApplication.class, args);
	}



//	@Bean
//	public CommandLineRunner setup(TweetRepository tweetRepository) {
//		return (args) -> {
//			tweetRepository.save(new Tweet("Meu primeiro Tweet!"));
//			tweetRepository.save(new Tweet("Meu segundo Tweet!"));
//			logger.info("The sample data has been generated");
//		};
//	}
}
