CREATE TABLE tweet (
 id integer AUTO_INCREMENT PRIMARY KEY,
 tweet VARCHAR(255) NOT NULL
);

CREATE TABLE user (
  id integer AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  bio TEXT NOT NULL
);

ALTER TABLE tweet ADD user_id integer NOT NULL DEFAULT 0;
ALTER TABLE tweet ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES user(id);

CREATE TABLE follows (
  follower_id integer NOT NULL,
  followed_id integer NOT NULL,
  FOREIGN KEY (follower_id) REFERENCES user (id),
  FOREIGN KEY (followed_id) REFERENCES user (id),
  PRIMARY KEY (follower_id, followed_id)
);